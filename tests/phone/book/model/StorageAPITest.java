/*
 * @author      apmishutkin@edu.hse.ru
 */

package phone.book.model;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.io.IOException;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

/**
 * Tests write to file and read from it
 */
class StorageAPITest {

    @Test
    void saveAndLoad() {
        // Yes, testing depends on proper work of Contact.of, that was tested previously and on correct work of file IO
        try {
            Contact contact = new Contact("a", "b", "c", "1", "2", "f", "g");
            Contact contact2 = new Contact("a", "b", "c", "13", "2", "df", "dg");
            Contact contact3 = new Contact("a", "b", "", "1", "2", "f", "g");

            StorageAPI.save(new File("kek.csv"), List.of(contact, contact2, contact3));
            List<Contact> list = StorageAPI.load(new File("kek.csv"));

            assertArrayEquals(new Contact[] {contact, contact2, contact3}, list.toArray());
        } catch (IOException ex) {
            fail("Test could be failed due to system error or because of bug in StorageAPI...");
        } catch (ContactValidateException cve) {
            fail("Something gone wrong");
        }
    }

    /**
     * Boyscout rule)
     */
    @AfterEach
    void afterEach() {
        try {
            File f = new File("kek.csv");
            if (f.exists()) {
                f.deleteOnExit();
            }
        } catch (Exception ignored) {

        }
    }
}