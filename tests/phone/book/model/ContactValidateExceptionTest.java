/*
 * @author      apmishutkin@edu.hse.ru
 */

package phone.book.model;

import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

/**
 * Again small test for read-only class
 */
class ContactValidateExceptionTest {
    @Test
    void getIndex() {
        ContactValidateException cve = new ContactValidateException(List.of(1), "Blah-blah-blah");
        assertArrayEquals(new Integer[]{1}, cve.getIndices().toArray());
    }
}