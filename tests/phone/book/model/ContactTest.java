/*
 * @author      apmishutkin@edu.hse.ru
 */

package phone.book.model;

import org.junit.jupiter.api.Test;

import java.util.Date;

import static org.junit.jupiter.api.Assertions.*;

/**
 * Simple tests to check correct work of <code>Contact</code> class
 *
 * <code>Contact</code> is read-only class, so only interesting method is it's builder
 */
class ContactTest {
    @Test
    void getters() {
        Date now = new Date();
        Date toCompare = new Date();
        toCompare.setTime(now.getTime());

        Contact contact = new Contact("a", "b", "c", "1234", "234", "f", now, "g");

        assertEquals("a", contact.getName());
        assertEquals("b", contact.getSurname());
        assertEquals("c", contact.getPatronymic());
        assertEquals("1234", contact.getMobilePhone());
        assertEquals("234", contact.getHomePhone());
        assertEquals("f", contact.getAddress());
        assertEquals(toCompare, contact.getBirthday());
        assertEquals("g", contact.getComment());
    }

    @Test
    void getFields() {
        Contact contact = new Contact("a", "b", "c", "1234", "234", "f", "g");

        assertArrayEquals(new String[]{"b", "a", "c", "1234", "234", "f", "", "g"}, contact.getFields());
    }

    @Test
    void getFieldsNames() {
        assertArrayEquals(new String[]{
                "Фамилия",
                "Имя",
                "Отчество",
                "Мобильный телефон",
                "Домашний телефон",
                "Адрес",
                "День рождения",
                "Комментарий"}, Contact.getFieldsNames());
    }

    @Test
    void of() {
        Contact contact = null;
        Contact contact2 = null;
        try {
            contact = Contact.of(new String[]{"b", "a", "c", "1234", "234", "f", "", "g"});
            contact2 = Contact.of(new String[]{"b", "a", "c", "1234", "234", "f", "11.12.1032", "g"});
        } catch (ContactValidateException contactValidateException) {
            fail();
        }

        assertArrayEquals(new String[]{"b", "a", "c", "1234", "234", "f", "", "g"}, contact.getFields());
        assertArrayEquals(new String[]{"b", "a", "c", "1234", "234", "f", "11.12.1032", "g"}, contact2.getFields());

        // Let's check all range of wrong Contact formats)
        assertThrows(ContactValidateException.class,
                () -> Contact.of(new String[]{"b", "a", "c", "1234", "234", "f", "11.12.3000", "g"}));
        assertThrows(ContactValidateException.class,
                () -> Contact.of(new String[]{"b", "a", "c", "1234", null, "f", "11.12.2000", "g"}));
        assertThrows(ContactValidateException.class,
                () -> Contact.of(new String[]{"   ", "  ", " ", "1234", "null", "f", "11.12.2000", "g"}));
        assertThrows(ContactValidateException.class,
                () -> Contact.of(new String[]{"   ", "  ", " ", "n", "11", "f", "11.12.2000", "g"}));
        assertThrows(ContactValidateException.class,
                () -> Contact.of(new String[]{"   ", "  ", " ", "1234", "null", "f", "", "g"}));
        assertThrows(ContactValidateException.class,
                () -> Contact.of(new String[]{""}));
        assertThrows(ContactValidateException.class,
                () -> Contact.of(new String[]{"b", "", "c", "1234", "234", "f", "11.12.2000", "g"}));
        assertThrows(ContactValidateException.class,
                () -> Contact.of(new String[]{"b", "a", "c", "", "", "", "11.12.3000", "g"}));
        assertThrows(ContactValidateException.class,
                () -> Contact.of(new String[]{"b", "a", "c", "1234", "234", "f", "KEK LOL", "g"}));
        assertThrows(ContactValidateException.class,
                () -> Contact.of(new String[]{"b", "a", "c", "5", "3", "", "11.12.3000", "<html>g</html>"}));
    }

    @Test
    void testEquals() {
        Contact contact = new Contact("a", "b", "c", "1234", "234", "f", "g");
        Contact contact2 = new Contact("a", "b", "c", "432", "432", "df", "dg");
        Contact contact3 = new Contact("a", "b", "", "1234", "234", "f", "g");

        assertEquals(contact, contact);
        assertEquals(contact, contact2);
        assertNotEquals(contact2, contact3);
        assertNotEquals(contact3, null);
    }

    @Test
    void testHashCode() {
        Contact contact = new Contact("a", "b", "c", "1234", "234", "f", "g");
        Contact contact2 = new Contact("a", "b", "c", "432", "432", "df", "dg");
        Contact contact3 = new Contact("a", "b", "", "1234", "234", "f", "g");

        assertEquals(contact.hashCode(), contact2.hashCode());
        assertNotEquals(contact2.hashCode(), contact3.hashCode());
    }

    @Test
    void diff() {
        Contact contact = new Contact("a", "b", "c", "1234", "234", "f", "g");
        Contact contact2 = new Contact("a", "b", "c", "432", "432", "df", "dg");
        Contact contact3 = new Contact("a", "b", "", "1234", "234", "f", "g");

        assertArrayEquals(new Integer[]{3, 4, 5, 7}, contact.diff(contact2).toArray());
        assertArrayEquals(new Integer[]{2}, contact.diff(contact3).toArray());
        assertArrayEquals(contact2.diff(contact).toArray(), contact.diff(contact2).toArray());
        assertArrayEquals(contact3.diff(contact2).toArray(), contact2.diff(contact3).toArray());
    }
}