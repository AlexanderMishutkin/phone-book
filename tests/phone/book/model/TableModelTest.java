/*
 * @author      apmishutkin@edu.hse.ru
 */

package phone.book.model;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

/**
 * Tests correct work with data by <code>TableModel</code>. Do not care about GUI
 *
 * Most of tests are foobar, but changing strategies in merge test and work with files is not trivial
 */
class TableModelTest {
    TableModel table;
    static File file = new File("phone_book.csv");
    static File export = new File("export_target.csv");

    /**
     * Creating test phone book by writing there raw CSV
     */
    @BeforeEach
    void createTable() {
        try (FileWriter writer = new FileWriter(file)) {
            writer.write("\"1\";\"1\";\"1\";\"1\";\"1\";\"1\";\"01.01.2021\";\"1\"\n" +
                    "\"2\";\"2\";\"2\";\"2\";\"2\";\"2\";\"\";\"2\"\n" +
                    "\"3\";\"3\";\"3\";\"3\";\"3\";\"3\";\"\";\"3\"\n");
        } catch (IOException ex) {
            fail("Test could be failed due to system error or because of bug in StorageAPI...");
        }

        table = new TableModel();
        assertTrue(table.isStorageExists());
    }

    /**
     * Boyscout rule)
     */
    @AfterEach
    void deleteStorage() {
        try {
            boolean b = false;
            if (file.exists()) {
                b = !file.delete();
            }
            if (b) {
                fail("Storage is blocked!");
            }
        } catch (Exception ignored) {

        }
    }

    /**
     * Boyscout rule)
     */
    @AfterAll
    static void deleteExportStorage() {
        try {
            boolean b = false;
            if (export.exists()) {
                b = !export.delete();
            }
            if (b) {
                fail("Storage is blocked!");
            }
        } catch (Exception ignored) {

        }
    }

    @Test
    void exportImportTable() {
        ArrayList<Contact> loaded = new ArrayList<>();

        // Test simple export
        try {
            table.exportTable(export);
        } catch (IOException e) {
            fail("Export failed!");
        }

        try {
            loaded = StorageAPI.load(export);
        } catch (IOException | ContactValidateException e) {
            fail("Export failed (could not read after import)!");
        }

        // Test simple import
        try {
            deleteStorage();
            table = new TableModel();
            table.importTable(export);
        } catch (IOException | ContactValidateException e) {
            fail("Import failed (could not read after import)!");
        }

        // Import and export are actually gathered to use two assertions instead of three
        assertArrayEquals(table.getFilteredContacts().toArray(), loaded.toArray());
        assertArrayEquals(loaded.toArray(), table.getFilteredContacts().toArray());
    }

    @Test
    void exportImportFilteredTable() {
        ArrayList<Contact> loaded = new ArrayList<>();

        // Test simple export
        try {
            table.exportFilteredTable(export); // Only difference from test above) But it is Important!
        } catch (IOException e) {
            fail("Export failed!");
        }

        try {
            loaded = StorageAPI.load(export);
        } catch (IOException | ContactValidateException e) {
            fail("Export failed (could not read after import)!");
        }

        // Test simple import
        try {
            deleteStorage(); // To make sure we import something, not read it from test phonebook
            table = new TableModel();
            table.importTable(export);
        } catch (IOException | ContactValidateException e) {
            fail("Import failed (could not read after import)!");
        }

        // Import and export are actually gathered to use two assertions instead of three
        assertArrayEquals(table.getFilteredContacts().toArray(), loaded.toArray());
        assertArrayEquals(loaded.toArray(), table.getFilteredContacts().toArray());
    }

    @Test
    void save() {
        ArrayList<Contact> loaded = new ArrayList<>();

        try {
            deleteStorage(); // To make sure we really change something
            table.save();
        } catch (IOException e) {
            fail("Export failed!");
        }

        try {
            loaded = StorageAPI.load(file);
        } catch (IOException | ContactValidateException e) {
            fail("Export failed (could not read after import)!");
        }

        assertArrayEquals(table.getFilteredContacts().toArray(), loaded.toArray());
    }

    @Test
    void replaceContact() {
        try {
            table.replaceContact(table.getFilteredContacts().get(0),
                    Contact.of(new Object[]{"1", "1", "1", "", "2", "", "", ""}));
            assertArrayEquals(new Object[]{"1", "1", "1", "", "2", "", "", ""},
                    table.getFilteredContacts().get(0).getFields());
        } catch (ContactValidateException contactValidateException) {
            fail("Error with Contact.of");
        }
    }

    @Test
    void removeAndContains() {
        Contact c = table.getFilteredContacts().get(0);
        assertTrue(table.contains(c));
        table.removeContact(c);
        assertFalse(table.contains(c));
        assertNotEquals(c, table.getFilteredContacts().get(0));
    }

    @Test
    void getRowCount() {
        assertEquals(3, table.getRowCount());
    }

    @Test
    void getColumnCount() {
        assertEquals(Contact.getFieldsNames().length, table.getColumnCount());
    }

    @Test
    void getValueAt() {
        assertEquals("01.01.2021", table.getValueAt(0, 6));
        assertEquals("2", table.getValueAt(1, 1));
        assertEquals("3", table.getValueAt(2, 2));
    }

    @Test
    void getColumnName() {
        for (int i = 0; i < Contact.getFieldsNames().length; i++) {
            // Column name is HTML. We need extract raw text from it
            assertEquals(Contact.getFieldsNames()[i], table.getColumnName(i)
                    .replaceAll("<[a-z/]+>", " ")
                    .replaceAll(" +", " ").trim());
        }
    }

    @Test
    void setFilter() {
        Contact two = table.getFilteredContacts().get(1);
        table.setFilter("2");
        assertArrayEquals(new Contact[]{two}, table.getFilteredContacts().toArray());
        table.setFilter("2");
        assertArrayEquals(new Contact[]{two}, table.getFilteredContacts().toArray());
    }

    @Test
    void getFilter() {
        assertEquals("", table.getFilter());
        table.setFilter("KEK LOL");
        assertEquals("KEK LOL", table.getFilter());
    }

    @Test
    void isStorageExists() {
        assertTrue(table.isStorageExists());

        // Test on creating new file
        deleteStorage();
        TableModel tm = new TableModel();
        assertTrue(tm.isStorageExists());

        // Test on reading wrong file
        try (FileWriter writer = new FileWriter(file)) {
            writer.write("\"1\";\"1\";\"1\";\"1\";\"1\";\"1\";\"01.01.3021\";\"1\"\n" +
                    "\"2\";\"2\";\"2\";\"2\";\"2\";\"2\";\"\";\"2\"\n" +
                    "\"3\";\"3\";\"3\";\"3\";\"3\";\"3\";\"\";\"3\"\n");
        } catch (IOException ex) {
            fail("Test could be failed due to system error or because of bug in StorageAPI...");
        }
        tm = new TableModel();
        assertFalse(tm.isStorageExists());
    }

    /**
     * Uses special merge strategy to test merge without user choice
     */
    @Test
    void mergeContacts() {
        table.setMergeStrategy((head, feature) -> MergeStrategy.ALWAYS_LEAVE_OLD);
        table.mergeContacts(List.of(new Contact("1", "1", "1", "2", "2", "2", "2"),
                new Contact("1", "1", "1", "3", "2", "2", "2")));
        assertEquals("1", table.getFilteredContacts().get(0).getAddress());

        table.setMergeStrategy((head, feature) -> MergeStrategy.ALWAYS_TAKE_NEW);
        table.mergeContacts(List.of(new Contact("1", "1", "1", "2", "2", "2", "2"),
                new Contact("1", "1", "1", "3", "2", "2", "2")));
        assertEquals("2", table.getFilteredContacts().get(0).getAddress());
    }
}