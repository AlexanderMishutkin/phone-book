/*
 * @author      apmishutkin@edu.hse.ru
 */

package phone.book.controll;

import phone.book.model.TableModel;
import phone.book.view.dialogs.MiscellaneousDialogs;

import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.IOException;

/**
 * Control main window and it's own events
 */
public class AppController {
    /**
     * Control closing of main event
     */
    public static class SaveTableAdapter extends WindowAdapter {
        private final TableModel tableModel;

        public SaveTableAdapter(TableModel tableModel) {
            this.tableModel = tableModel;
        }

        /**
         * Invoked when a window is in the process of being closed.
         * The close operation can be overridden at this point.
         *
         * @param e WindowEvent
         */
        @Override
        public void windowClosing(WindowEvent e) {
            try {
                tableModel.save();
            } catch (IOException ex) {
                MiscellaneousDialogs.showBackupError();
            }
            super.windowClosing(e);
        }

    }
}
