/*
 * @author      apmishutkin@edu.hse.ru
 */

package phone.book.model;

import phone.book.view.utils.MarginAndFonts;
import phone.book.view.dialogs.MergeDialog;

import javax.swing.table.AbstractTableModel;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import static phone.book.model.MergeStrategy.*;

/**
 * Model for <code>PhoneBookTable</code>
 */
public class TableModel extends AbstractTableModel {
    private final List<Contact> contacts;
    private final File source = new File("phone_book.csv");
    private String filter = "";
    private List<Contact> filteredContacts = null;
    private boolean storageExists;
    private MergeStrategy mergeStrategy;

    public TableModel() {
        super();
        contacts = new ArrayList<>();
        fireTableDataChanged();

        try {
            if (!source.exists() && !source.createNewFile()) {
                storageExists = false;
                return;
            }
            importTable(source);
            storageExists = true;
        } catch (IOException | ContactValidateException ex) {
            storageExists = false;
        }

        setMergeStrategy(MergeDialog::showMergeDialog);
    }

    /**
     * Imports contacts from file
     *
     * @param src file with contacts
     * @throws IOException if something wrong with file
     * @throws ContactValidateException if format of data in file is wrong
     */
    public void importTable(File src) throws IOException, ContactValidateException {
        filter = "";
        fireTableDataChanged();
        mergeContacts(StorageAPI.load(src));
    }

    /**
     * Export contacts from table into .csv file
     *
     * @param dest file to export contacts
     * @throws IOException if something wrong with file
     */
    public void exportTable(File dest) throws IOException {
        StorageAPI.save(dest, contacts);
    }

    /**
     * Export contacts from table into .csv file
     *
     * Import only displayed contacts
     *
     * @param dest file to export contacts
     * @throws IOException if something wrong with file
     */
    public void exportFilteredTable(File dest) throws IOException {
        StorageAPI.save(dest, filteredContacts);
    }

    /**
     * Export table data to default file
     *
     * @throws IOException if something wrong with file
     */
    public void save() throws IOException {
        StorageAPI.save(source, contacts);
    }

    /**
     * Replace given contact
     *
     * @throws IndexOutOfBoundsException if element is not in table
     * @param oldContact contact to replace
     * @param newContact contact to replace with
     */
    public void replaceContact(Contact oldContact, Contact newContact) throws IndexOutOfBoundsException {
        int i = contacts.indexOf(oldContact);
        contacts.set(i, newContact);
        fireTableDataChanged();
    }

    /**
     * Removes contact from table
     *
     * @param contact to remove
     */
    public void removeContact(Contact contact) {
        int i = -1;
        if (filteredContacts.contains(contact)) {
            i = filteredContacts.indexOf(contact);
        }
        contacts.remove(contact);
        if (i != -1) {
            fireFilterChanged();
            fireTableRowsDeleted(i, i);
        }
    }

    /**
     * Returns the number of rows in the model. A
     * <code>JTable</code> uses this method to determine how many rows it
     * should display.  This method should be quick, as it
     * is called frequently during rendering.
     *
     * @return the number of rows in the model
     * @see #getColumnCount
     */
    @Override
    public int getRowCount() {
        return filteredContacts.size();
    }

    /**
     * Returns the number of columns in the model. A
     * <code>JTable</code> uses this method to determine how many columns it
     * should create and display by default.
     *
     * @return the number of columns in the model
     * @see #getRowCount
     */
    @Override
    public int getColumnCount() {
        return Contact.getFieldsNames().length;
    }

    /**
     * Returns the value for the cell at <code>columnIndex</code> and
     * <code>rowIndex</code>.
     *
     * @param rowIndex    the row whose value is to be queried
     * @param columnIndex the column whose value is to be queried
     * @return the value Object at the specified cell
     */
    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        return filteredContacts.get(rowIndex).getFields()[columnIndex];
    }

    /**
     * @param column column number
     * @return name of column
     */
    @Override
    public String getColumnName(int column) {
        return MarginAndFonts.headerToHTML(Contact.getFieldsNames()[column]);
    }

    /**
     * @return contacts with filter applied
     */
    public List<Contact> getFilteredContacts() {
        return filteredContacts;
    }

    /**
     * @param filter current filter as string
     */
    public void setFilter(String filter) {
        if (filter.equals(this.filter)) {
            return;
        }

        this.filter = filter;
        fireTableDataChanged();
    }

    /**
     * Update filtered list if filter changes
     */
    public void fireFilterChanged() {
        this.filteredContacts = contacts.stream()
                .filter((Contact c) -> c.getName().contains(filter) || c.getSurname().contains(filter) ||
                        c.getPatronymic().contains(filter)).collect(Collectors.toList());
    }

    @Override
    public void fireTableDataChanged() {
        fireFilterChanged();
        super.fireTableDataChanged();
    }

    /**
     * @return filter - as query string
     */
    public String getFilter() {
        return filter;
    }

    /**
     * Was phone book loaded successfully in constructor
     *
     * @return true if phone book loaded successfully in constructor
     */
    public boolean isStorageExists() {
        return storageExists;
    }

    /**
     * Same as <code>List</code> contains
     */
    public boolean contains(Contact contact) {
        return contacts.contains(contact);
    }

    /**
     * Adds to table new contacts, checking for same contacts
     *
     * @param newContacts contacts to add in table
     */
    public void mergeContacts(List<Contact> newContacts) {
        int choice = 0;

        for (Contact newContact : newContacts) {
            int j = contacts.indexOf(newContact);
            if (j >= 0) {
                if (choice == ALWAYS_LEAVE_OLD) {
                    continue;
                }
                if (choice == ALWAYS_TAKE_NEW) {
                    contacts.set(j, newContact);
                }


                ArrayList<Integer> diff = contacts.get(j).diff(newContact);
                if (diff != null && diff.size() > 0) {
                    choice = getMergeStrategy().chooseContact(contacts.get(j), newContact);

                    if (choice == LEAVE_OLD || choice == ALWAYS_LEAVE_OLD) {
                        continue;
                    }
                    if (choice == TAKE_NEW || choice == ALWAYS_TAKE_NEW) {
                        contacts.set(j, newContact);
                    }
                }
            } else {
                contacts.add(newContact);
            }
        }

        fireTableDataChanged();
    }

    public MergeStrategy getMergeStrategy() {
        return mergeStrategy;
    }

    public void setMergeStrategy(MergeStrategy mergeStrategy) {
        this.mergeStrategy = mergeStrategy;
    }
}
