/*
 * @author      apmishutkin@edu.hse.ru
 */

package phone.book.model;

/**
 * Describes strategy of contacts merging
 *
 * Created to be possible to test merging)
 */
public interface MergeStrategy {
    int LEAVE_OLD = 0;
    int TAKE_NEW = 1;
    int ALWAYS_LEAVE_OLD = 2;
    int ALWAYS_TAKE_NEW = 3;

    /**
     * Chooses <code>Contact</code> to keep in table. Return one of constants
     *
     * @param head old <code>Contact</code>
     * @param feature new <code>Contact</code>
     * @return which <code>Contact</code> to chose
     */
    int chooseContact(Contact head, Contact feature);
}
