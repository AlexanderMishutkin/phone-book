/*
 * @author      apmishutkin@edu.hse.ru
 */

package phone.book.model;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.DateTimeException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Objects;

/**
 * Class representing a phone book row - information about single person.
 */
public class Contact {
    private final String name;
    private final String surname;
    private final String patronymic;
    private final String mobilePhone;
    private final String homePhone;
    private final String address;
    private final Date birthday;
    private final String comment;

    Contact(String name, String surname, String patronymic, String mobilePhone, String homePhone, String address,
            String comment) {
        this(name, surname, patronymic, mobilePhone, homePhone, address, null, comment);
    }

    Contact(String name, String surname, String patronymic, String mobilePhone, String homePhone, String address,
            Date birthday, String comment) {
        this.name = name;
        this.surname = surname;
        this.patronymic = patronymic;
        this.mobilePhone = mobilePhone;
        this.homePhone = homePhone;
        this.address = address;
        this.birthday = birthday;
        this.comment = comment;
    }

    /**
     * Common getter
     *
     * @return fieldsValue
     */
    public String getName() {
        return name;
    }

    /**
     * Common getter
     *
     * @return fieldsValue
     */
    public String getSurname() {
        return surname;
    }

    /**
     * Common getter
     *
     * @return fieldsValue
     */
    public String getPatronymic() {
        return patronymic;
    }

    /**
     * Common getter
     *
     * @return fieldsValue
     */
    public String getMobilePhone() {
        return mobilePhone;
    }

    /**
     * Common getter
     *
     * @return fieldsValue
     */
    public String getAddress() {
        return address;
    }

    /**
     * Common getter
     *
     * @return fieldsValue
     */
    public Date getBirthday() {
        return birthday;
    }

    /**
     * Common getter
     *
     * @return fieldsValue
     */
    public String getComment() {
        return comment;
    }

    /**
     * Common getter
     *
     * @return fieldsValue
     */
    public String getHomePhone() {
        return homePhone;
    }

    /**
     * Represents all fields of contact as array of <code>String</code> converted to <code>Object</code>
     * for usage in <code>TableModel</code>
     * <p>
     * Order of fields is <pre>
     *     surname,
     *     name,
     *     patronymic,
     *     mobilePhone,
     *     homePhone,
     *     address,
     *     birthday,
     *     comment
     * </pre>
     *
     * @return fields converted to <code>String</code>
     */
    public Object[] getFields() {
        return new Object[]{
                surname,
                name,
                patronymic,
                mobilePhone,
                homePhone,
                address,
                birthday != null ? FORMATTER.format(birthday) : "",
                comment};
    }

    /**
     * Represents names of all fields of contact as array of <code>String</code> (in Russian language)
     * for usage in <code>TableModel</code>
     * <p>
     * Order of fields is <pre>
     *     surname,
     *     name,
     *     patronymic,
     *     mobilePhone,
     *     homePhone,
     *     address,
     *     birthday,
     *     comment
     * </pre>
     *
     * @return field's names converted to <code>String</code>
     */
    static public String[] getFieldsNames() {
        return new String[]{
                "Фамилия",
                "Имя",
                "Отчество",
                "Мобильный телефон",
                "Домашний телефон",
                "Адрес",
                "День рождения",
                "Комментарий"};
    }

    /**
     * Format date to the <code>dd.MM.yyyy</code> pattern
     */
    static public final SimpleDateFormat FORMATTER = new SimpleDateFormat("dd.MM.yyyy");

    /**
     * Builds <code>Contact</code> from array of it's fields' values.
     * <p>
     * Values should be convertible to <code>String</code> their order is <pre>
     *     surname,
     *     name,
     *     patronymic,
     *     mobilePhone,
     *     homePhone,
     *     address,
     *     birthday,
     *     comment
     * </pre>
     * <p>
     * Validates each field value as it is described in design
     *
     * @param array fields' values
     * @return new <code>Contact</code>
     * @throws ContactValidateException if some field has not appropriating data
     */
    public static Contact of(Object[] array) throws ContactValidateException {
        ArrayList<Integer> errors = new ArrayList<>();
        StringBuilder msg = new StringBuilder();

        if (array.length < getFieldsNames().length) {
            throw new ContactValidateException(errors, " ∙ " + ContactValidateException.NOT_ENOUGH_FIELDS);
        }

        for (int i = 0; i < array.length; i++) {
            if (!(array[i] instanceof String)) {
                errors.add(i);
                msg.append(" ∙ " + ContactValidateException.NOT_STRING_ELEMENT_PASSED);
                msg.append(System.lineSeparator());
            }
        }

        if (!errors.isEmpty()) {
            throw new ContactValidateException(errors, msg.toString());
        }

        for (int i = 0; i < array.length; i++) {
            if (array[i].toString().replace(" ", "").contains("</") ||
                    array[i].toString().replace(" ", "").contains(">")) {
                errors.add(i);
                msg.append(" ∙ " + ContactValidateException.HTML_INJECTION);
                msg.append(System.lineSeparator());
            }
        }

        String surname = (String) array[0];
        String name = (String) array[1];

        if (surname.isBlank()) {
            errors.add(0);
            msg.append(" ∙ " + ContactValidateException.NECESSARY_FIELD_IS_EMPTY);
            msg.append(System.lineSeparator());
        }
        if (name.isBlank()) {
            errors.add(1);
            msg.append(" ∙ " + ContactValidateException.NECESSARY_FIELD_IS_EMPTY);
            msg.append(System.lineSeparator());
        }

        String patronymic = (String) array[2];
        String mobilePhone = (String) array[3];
        String homePhone = (String) array[4];
        String address = (String) array[5];
        String comment = (String) array[7];
        Date birthday;

        if (homePhone.isBlank() && mobilePhone.isBlank()) {
            errors.add(3);
            errors.add(4);
            msg.append(" ∙ " + ContactValidateException.NO_PHONE_GIVEN);
            msg.append(System.lineSeparator());
        }

        if (!mobilePhone.matches("^[0-9]*$")) {
            errors.add(3);
            msg.append(" ∙ " + ContactValidateException.LETTER_IN_PHONE);
            msg.append(System.lineSeparator());
        }

        if (!homePhone.matches("^[0-9]*$")) {
            errors.add(4);
            msg.append(" ∙ " + ContactValidateException.LETTER_IN_PHONE);
            msg.append(System.lineSeparator());
        }

        if (array[6].toString().isBlank()) {
            if (!errors.isEmpty()) {
                throw new ContactValidateException(errors, msg.toString());
            }

            return new Contact(name, surname, patronymic, mobilePhone, homePhone, address, comment);
        }

        try {
            birthday = FORMATTER.parse((String) array[6]);
            if (birthday.after(new Date())) {
                errors.add(6);
                msg.append(" ∙ " + ContactValidateException.BORN_IN_FUTURE);
                msg.append(System.lineSeparator());
            }
        } catch (DateTimeException | ParseException dte) {
            throw new ContactValidateException(List.of(6), " ∙ " + ContactValidateException.BAD_DATE_FORMAT);
        }

        if (!errors.isEmpty()) {
            throw new ContactValidateException(errors, msg.toString());
        }

        return new Contact(name, surname, patronymic, mobilePhone, homePhone, address, birthday, comment);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Contact contact = (Contact) o;
        return getName().equals(contact.getName()) &&
                getSurname().equals(contact.getSurname()) &&
                getPatronymic().equals(contact.getPatronymic());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getName(), getSurname(), getPatronymic());
    }

    /**
     * Gives fields that are different between two contacts
     * <p>
     * Order ov fields is <pre>
     *     surname,
     *     name,
     *     patronymic,
     *     mobilePhone,
     *     homePhone,
     *     address,
     *     birthday,
     *     comment
     * </pre>
     *
     * @param contact contact to compare
     * @return list of different field indices
     */
    public ArrayList<Integer> diff(Contact contact) {
        ArrayList<Integer> d = new ArrayList<>();

        for (int i = 0; i < contact.getFields().length; i++) {
            if (!contact.getFields()[i].equals(getFields()[i])) {
                d.add(i);
            }
        }
        return d;
    }
}
