/*
 * @author      apmishutkin@edu.hse.ru
 */

package phone.book.view.utils;

import javax.swing.*;

public class Localization {
    /**
     * Translate some labels in russian
     */
    static public void localize() {
        // JFileChooser localization
        UIManager.put(
                "FileChooser.saveButtonText", "Сохранить");
        UIManager.put(
                "FileChooser.openButtonText", "Открыть");
        UIManager.put(
                "FileChooser.cancelButtonText", "Отмена");
        UIManager.put(
                "FileChooser.fileNameLabelText", "Наименование файла");
        UIManager.put(
                "FileChooser.filesOfTypeLabelText", "Типы файлов");
        UIManager.put(
                "FileChooser.lookInLabelText", "Директория");
        UIManager.put(
                "FileChooser.saveInLabelText", "Сохранить в директории");
        UIManager.put(
                "FileChooser.folderNameLabelText", "Путь директории");
        UIManager.put(
                "FileChooser.openDialogTitleText", "Импортировать контакты");
        UIManager.put(
                "FileChooser.saveDialogTitleText", "Экспортировать таблицу");
        UIManager.put(
                "FileChooser.acceptAllFileFilterText", "Любое разрешение");
    }
}
