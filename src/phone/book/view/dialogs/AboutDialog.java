/*
 * @author      apmishutkin@edu.hse.ru
 */

package phone.book.view.dialogs;

import phone.book.controll.MenuController;
import phone.book.view.utils.MarginAndFonts;

import javax.swing.*;
import java.awt.*;

/**
 * Gives information about author of program
 *
 * A small eastern egg is hidden in this information
 */
public class AboutDialog extends JFrame {

    private AboutDialog() {
        super("О программе");
        JPanel contentPane = (JPanel) getContentPane();
        contentPane.setLayout(new BorderLayout());

        JLabel label = new JLabel(
                "<html>" +
                        "<center><h1>Программа \"телефонная книга\"</h1></center>" +
                        "<center><h2>Автор: Мишуткин Александр Петрович, БПИ-192</h2></center>" +
                        "<hr/>" +
                        "<i color='gray'>Вместо скучного послания предкам, я оставлю им стремную " +
                        "<b color='red'>кнопку самоликвидации</b>" +
                        "(!)<br/>Выяснить, что именно делает эта кнопка, вы можете двумя способами:<br/>" +
                        "<ol><li>Проверить код класса <tt>MenuController.TotalSelfDestructionHandler</tt></li>" +
                        "<li>Нажать)))</li></ol></i>" +
                        "</html>"
        );

        contentPane.add(MarginAndFonts.setMargin(label, 20), BorderLayout.NORTH);
        JButton selfDestroy = new JButton(
                "<html><h1>САМОЛИКВИДАЦИЯ!</h1></html>");
        selfDestroy.setBackground(new Color(255, 0, 0));
        selfDestroy.addActionListener(new MenuController.TotalSelfDestructionHandler());

        contentPane.add(MarginAndFonts.setMargin(selfDestroy, 30), BorderLayout.CENTER);
        pack();
        setVisible(true);
    }

    /**
     * Shows non modal window describing program and it's author
     */
    public static void showAboutDialog() {
        new AboutDialog();
    }
}
