/*
 * @author      apmishutkin@edu.hse.ru
 */

package phone.book.view.dialogs;

import phone.book.controll.MergeDialogController;
import phone.book.model.Contact;
import phone.book.controll.FrameHierarchyController;
import phone.book.view.utils.MarginAndFonts;

import javax.swing.*;
import java.awt.*;
import java.awt.event.WindowEvent;
import java.util.List;

/**
 * Dialog describing changes between two Contacts and giving opportunity to take one
 */
public class MergeDialog extends JDialog {
    private final JCheckBox checkBox = new JCheckBox("Для всех контактов");
    private int choice = 0;

    private MergeDialog(Contact head, Contact feature, List<Integer> diff) {
        super(FrameHierarchyController.getMainFrame(), "Повторяющиеся контакты", true);
        JPanel contentPane = (JPanel) getContentPane();
        contentPane.setLayout(new BorderLayout());

        JPanel footer = new JPanel();
        createFooter(footer);

        JPanel diffPanel = new JPanel(new GridLayout(Contact.getFieldsNames().length + 2, 4, 5, 5));

        createPanelWithDifferences(head, feature, diff, diffPanel);

        diffPanel.add(checkBox);

        contentPane.add(new JLabel("В таблице уже есть такой же контакт, но его данные отличаются." +
                " Как быть с его данными?"), BorderLayout.NORTH);
        contentPane.add(MarginAndFonts.setMargin(diffPanel, 5), BorderLayout.CENTER);
        contentPane.add(footer, BorderLayout.SOUTH);

        pack();
        setVisible(true);
    }

    /**
     * Actually creates common dialog footer
     *
     * @param footer JPanel for buttons
     */
    private void createFooter(JPanel footer) {
        footer.setLayout(new BoxLayout(footer, BoxLayout.X_AXIS));
        JButton oldData = new JButton("Оставить старые");
        oldData.addActionListener(new MergeDialogController.TakeOldHandler(this));
        JButton newData = new JButton("Взять новые");
        newData.addActionListener(new MergeDialogController.TakeNewHandler(this));

        footer.add(oldData);
        footer.add(newData);
    }

    /**
     * Creates grid, showing differences between contacts
     *
     * @param head old Contact
     * @param feature new Contact
     * @param diff differences as indices of fields
     * @param diffPanel grid panel
     */
    private void createPanelWithDifferences(Contact head, Contact feature, List<Integer> diff, JPanel diffPanel) {
        diffPanel.add(new JLabel(""));
        diffPanel.add(MarginAndFonts.boldFont(new JLabel("Старое")));
        diffPanel.add(new JLabel(""));
        diffPanel.add(MarginAndFonts.boldFont(new JLabel("Новое")));

        for (int i = 0; i < Contact.getFieldsNames().length; i++) {
            diffPanel.add(MarginAndFonts.boldFont(new JLabel(Contact.getFieldsNames()[i])));
            if (diff.contains(i)) {
                diffPanel.add(new JLabel(head.getFields()[i].toString()));
                diffPanel.add(new JLabel(""));
                diffPanel.add(new JLabel(feature.getFields()[i].toString()));
            } else {
                diffPanel.add(new JLabel(""));
                diffPanel.add(new JLabel(head.getFields()[i].toString()));
                diffPanel.add(new JLabel(""));
            }
        }
    }

    /**
     * Shows modal dialog describing changes between two Contacts and giving opportunity to take one.
     *
     * Can be used as merge strategy
     *
     * @param head old Contact
     * @param feature new Contact
     * @return choice (described as MergeStrategy constants)
     */
    public static int showMergeDialog(Contact head, Contact feature) {
        MergeDialog mergeDialog = new MergeDialog(head, feature, head.diff(feature));
        return mergeDialog.choice;
    }

    public void setChoice(int choice) {
        this.choice = choice;
    }

    public JCheckBox getCheckBox() {
        return this.checkBox;
    }

    public void exit() {
        processWindowEvent(new WindowEvent(this, WindowEvent.WINDOW_CLOSING));
    }
}
