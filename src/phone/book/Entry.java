/*
 * @author      apmishutkin@edu.hse.ru
 */

package phone.book;

import phone.book.view.MainWindow;

/**
 * It is simple entry point, due to primitive work of Swing, it only creates object of MainWindow class
 * <p>
 * The projects works on Swing. App consists of main window, add/edit form and a couple of dialogs.
 * The project structure implements MVC pattern, where View displays data, provided by Model,
 * and View components uses event handlers, provided by Controller. This scheme describes project in details
 * <p>
 * PS. use reading mode (CTRL+ALT+Q) to enjoy it!
 *
 * <pre>
 * ....┌────────────────────────────────────────┐
 * ....│Contact...............ContactValidateEx.│
 * ....│....│of.───────────────.................│
 * ....│....│.......throws......................│
 * ....│StorageAPI..............................│
 * ....│....│save...............................│
 * ....│....│.....────────────..................│
 * .┌──┤TableModel..uses......MergeStrategy.....│
 * .│..└─────────┬──────────────────────────────┘
 * .│............│
 * .│............│..provides.data
 * .│changes.....└────────────────────┐
 * .│.................................│
 * .│..┌────┬─────────────────────────┼──────┬───────┐
 * .│..│VIEW│.........................│......├───────┤
 * .│..├────┘.........................│......│Dialogs├──┐
 * .│..│Footer.Form.Header.MainWindow.Table..├───────┤..│
 * .│..└───┬─────┬─┬─┬──┬─────┬──────────────┴─┼─────┘..│
 * .│......│.....│.│.│..│.....│................│........│
 * .│......│.....│.└┬┘..│.....│.....┌──────────┘........│
 * .│......│.....│..│...│.....│.....│...................│
 * .│......│.....│..│...│.....│.....│..enable.items.....│
 * .│......│.....│..└───┴─────┼─────┼──────────┐........│
 * .│......│provides.handlers.│.....│..........│........│
 * .│..┌───┴──────────────────┴─────┴──────────┴──────┐.│
 * .└──┤FooterC.FormC.MenuC.AppC..MergeC...SelectionC.│.│
 * ....│..............................................│.│
 * ....│FrameHierarchyC.──────────────────────────────┼─┘
 * ....├──────────┐..........provides.parent..........│
 * ....│CONTROLLER│...................................│
 * ....└──────────┴───────────────────────────────────┘
 * </pre>
 */
public class Entry {
    public static void main(String[] args) {
        new MainWindow();
    }
}
